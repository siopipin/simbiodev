const { teams } = require('../../variables')
const variables = require('../../variables')
const async = require('async')
const conn = require('./db')
exports.get_list_qc_customer = (req, res) => {
    var id = req.params.id
    var query = "select a.id, c.id as id_qc, a.id_order as kode_order, a.nama_pasien, b.nama as customer, a.tanggal_selesai, c.warranty, c.date as last_updated_qc, c.team from tbl_order a LEFT JOIN tbl_customers b ON a.id_customer = b.id INNER JOIN tbl_qc c ON a.id = c.id_order WHERE a.id_customer = ? order by a.tanggal_selesai DESC;"
    conn.query(query, [id], (err, rows) => {
        if (err) res.status(400).json(err)
        else {
            for (let item of rows) {
                item.warranty = item.warranty == 2 ? 'Eligible' : 'Not Eligible'
                item.team = teams[item.team - 1].nama
            }
            res.json(rows)
        }
    })
}
exports.get_quality_control_single = (req, res) => {
    var id = req.params.id
    conn.query("select b.id, a.nama_pasien, c.nama as customer, a.id_order as kode_order, b.anatomi, b.surface, b.shade, b.fitting, b.occlusion, b.warranty, b.team from tbl_order a INNER JOIN tbl_qc b ON a.id = b.id_order LEFT JOIN tbl_customers c ON a.id_customer = c.id WHERE b.id_order = " + id, (err, row) => {
        if (row.length <= 0) res.status(403).json({})
        else {
            var item = row[0]
            item.team = teams[item.team - 1].nama
            item.warranty = item.warranty == 2 ? 'Eligible' : 'Not Eligible'
            res.status(200).json(item)
        }

    })
}


//Review
exports.review_allow = (req, res) => {
    var idorder = req.params.idorder
    conn.query(
        "SELECT jobdesk.id_order FROM tbl_jobdesk jobdesk LEFT JOIN tbl_order ord ON jobdesk.id_order = ord.id_order WHERE  jobdesk.id_order = " + idorder,
        (err, result) => {
            if (err) res.status(400).json(err)
            else res.json(result)
        },
    )
}

exports.review_order = (req, res) => {
    var idorder = req.params.idorder

    conn.query("select a.id, a.id_order, ((a.rating_warna + a.rating_anatomi + a.rating_fitting + a.rating_oklusi + a.rating_komunikasi) / 5) as rating_overall, a.rating_warna, a.rating_anatomi, a.rating_fitting, a.rating_oklusi, a.rating_komunikasi, b.id_order as no_order, c.nama as customer, a.catatan, jobdesk.followup, a.date from tbl_evaluasi a LEFT JOIN tbl_order b ON a.id_order = b.id  LEFT JOIN tbl_jobdesk jobdesk ON b.id_order = jobdesk.id_order LEFT JOIN tbl_customers c ON b.id_customer = c.id where a.id_order = " + idorder, (err, rows) => {
        let items = rows
        async.eachSeries(items, (item, cb) => {
            conn.query("select * from tbl_evaluasi_foto where id_evaluasi = " + item.id, (err, row) => {
                item.images = row
                cb(null)
            })
        }, error => {
            console.log(items);
            if (items.length == 0) {
                //403 data not found
                res.status(403).json(items[0])
            } else {
                res.status(200).json(items[0])

            }
        })
    })
}


function formatDate(date) {
    var dt = new Date(date)
    return dt.getFullYear() + '-' + (dt.getMonth() < 9 ? '0' : '') + (dt.getMonth() + 1) + '-' + (dt.getDate() < 10 ? '0' : '') + dt.getDate();
}

exports.get_evaluasi = (req, res) => {
    conn.query("select distinct a.id, a.id_order, a.tanggal as tgl_masuk, a.tanggal_selesai as tgl_selesai, b.tgl_target_selesai, b.team, c.nama as customer from tbl_order a LEFT JOIN tbl_jobdesk b ON a.id_order = b.id_order LEFT JOIN tbl_customers c ON a.id_customer = c.id LEFT JOIN tbl_evaluasi d ON d.id_order = a.id WHERE a.status_invoice = 2 and a.tanggal >= '2022-01-01' and d.id is null", (err, rows)=>{
        var items = []
        for(let item of rows){
            let selected = items.filter(it=> it.id == item.id)
            if(selected.length>0){
                let index = items.indexOf(selected[0])
                items[index].nama_team += item.team ? ", " + variables.teams[item.team-1].nama : ''
            }else{
                item.nama_team = item.team ? variables.teams[item.team-1].nama : ''
                items.push(item)
            }
        }
        res.json(items)
    })
}

//Submit review & rating.
exports.review_add = (req, res) => {
    name_order = req.body.name_order;
    id_customer = req.body.id_customer;
    id_order = req.body.id_order;

    var data = {
        id_order: id_order,
        rating_warna: req.body.rating_warna,
        rating_fitting: req.body.rating_fitting,
        rating_oklusi: req.body.rating_oklusi,
        rating_anatomi: req.body.rating_anatomi,
        rating_komunikasi: req.body.rating_komunikasi,
        catatan: req.body.catatan
    }

    let images = []
    if (req.files) {
        for (let file of req.files) images.push(file.filename)
    }

    conn.query("INSERT INTO tbl_evaluasi SET ?", data, (err, result) => {
        var id_order = req.body.id_jobim
        conn.query("update tbl_jobdesk set followup = 1 where id_order = '" + name_order + "'", (err, rlst_) => {
            //select idcustomer
            console.log(`id_customer: ${id_customer}`);
            var query =
                'select * from tbl_customers where id = ?'
            conn.query(
                query,
                [id_customer],
                (err, res1) => {
                    console.log(`select tbl_customers: ${res1[0]}`);
                    console.log(`point cust: ${res1[0].poin}`);
                    var ttl = parseInt(res1[0].poin) + parseInt(25000)
                    console.log(`ttl point: ${ttl}`);

                    conn.query("UPDATE tbl_customers SET poin = '" + ttl + "' where id = '" + id_customer + "'", (err, res2) => {
                        console.log(`UPDATE tbl customer: ${res2}`);

                        //Insert history point
                        var query =
                            'INSERT INTO tbl_history_poin SET ?'
                        console.log(`idcust: ${req.body.id_customer}, idorder ${req.body.id_order}`);

                        conn.query(
                            query,
                            {
                                id_customer: req.body.id_customer,
                                id_order: req.body.id_order,
                                poin: 25000,
                                flag: 3,
                                addtional: 'review',
                            },
                            (err, res3) => {
                            })
                    })
                })
        })


        if (err) res.status(400).json(err)
        else {
            var id = result.insertId
            if (images.length <= 0) res.json(result)
            else {
                let data_images = []
                for (let image of images) data_images.push([id, image])
                conn.query("INSERT INTO tbl_evaluasi_foto (id_evaluasi, image) VALUES ?", [data_images], (err, rlst) => {
                    res.json(result)
                })
            }
        }
    })
}


