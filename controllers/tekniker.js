const conn = require("./db");
const async = require("async");

exports.get_orders = (req, res, next) => {
  var id_user = req.userData.id;

  var sqlCekOrder =
    "select a.privilege, b.id, b.id_order, b.id_customer, b.nama_pasien, c.nama as customer, b.tanggal, b.tanggal_target_selesai, b.status from tbl_spk a LEFT JOIN tbl_order b ON a.id_order = b.id LEFT JOIN tbl_customers c ON b.id_customer = c.id WHERE a.id_pegawai = " +
    id_user +
    " and not b.id is NULL ORDER BY b.tanggal_target_selesai ASC";

  conn.query(sqlCekOrder, (err, rows) => {
    var order = [];
    var items = rows;
    async.eachSeries(
      items,
      function (item, cb) {
        var sqlCekProduct =
          "select a.id, a.id_order, a.id_product, b.nama as nama_product, a.warna, a.posisi_gigi as posisi, a.harga as harga_product, a.poin, a.garansi, c.nama as bahan_product, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product and status = 2) as selesai, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product ) as total_pekerjaan, a.status from tbl_order_detail a, tbl_product b, tbl_bahan c WHERE c.id = b.id_bahan AND b.id = a.id_product AND a.id_order = " +
          item.id +
          " and a.status !=2 order by a.posisi_gigi asc";
        conn.query(sqlCekProduct, (err, row) => {
          if (err) {
            return res.status(400).send({
              status: 400,
            });
          } else {
            if (row.length != 0) {
              details = [];
              for (let item of row) {
                var selected = details.filter(
                  (it) =>
                    it.warna == item.warna && it.id_product == item.id_product
                );
                if (selected.length > 0) {
                  var index = details.indexOf(selected[0]);
                  details[index].posisi.push({
                    pos: item.posisi,
                  });
                } else {
                  var data = {
                    id_product: item.id_product,
                    nama_product: item.nama_product,
                    bahan_product: item.bahan_product,
                    warna: item.warna,

                    selesai: item.selesai,
                    total_pekerjaan: item.total_pekerjaan,
                    status: item.status,
                    posisi: [
                      {
                        pos: item.posisi,
                      },
                    ],
                  };
                  details.push(data);
                }
              }
              item.products = details;
              order.push(item);
              cb();
            } else {
              cb();
            }
          }
        });
      },
      function (e) {
        if (e) {
          return res.status(400).send({
            message: "cannot get data",
            data: order,
          });
        } else {
          return res.status(200).send({
            message: "successfully",
            orders: order,
          });
        }
      }
    );
  });
};

exports.get_orders_infinite = (req, res, next) => {
  var id_user = req.userData.id;
  var start = parseInt(req.query.start);
  var limit = parseInt(req.query.limit);

  var sqlCekOrder =
    "select a.privilege, b.id, b.id_order, b.id_customer, b.nama_pasien, c.nama as customer, b.tanggal, b.tanggal_target_selesai, b.status from tbl_spk a LEFT JOIN tbl_order b ON a.id_order = b.id LEFT JOIN tbl_customers c ON b.id_customer = c.id WHERE a.id_pegawai = " +
    id_user +
    " and not b.id is NULL ORDER BY b.tanggal_target_selesai DESC LIMIT ?,?";

  conn.query(sqlCekOrder, [start, limit], (err, rows) => {
    var order = [];
    var items = rows;
    async.eachSeries(
      items,
      function (item, cb) {
        var sqlCekProduct =
          "select a.id, a.id_order, a.id_product, b.nama as nama_product, a.warna, a.posisi_gigi as posisi, a.harga as harga_product, a.poin, a.garansi, c.nama as bahan_product, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product and status = 2) as selesai, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product ) as total_pekerjaan, a.status from tbl_order_detail a, tbl_product b, tbl_bahan c WHERE c.id = b.id_bahan AND b.id = a.id_product AND a.id_order = " +
          item.id +
          " and a.status !=2 order by a.posisi_gigi asc";
        conn.query(sqlCekProduct, (err, row) => {
          if (err) {
            return res.status(400).send({
              status: 400,
            });
          } else {
            if (row.length != 0) {
              details = [];
              for (let item of row) {
                var selected = details.filter(
                  (it) =>
                    it.warna == item.warna && it.id_product == item.id_product
                );
                if (selected.length > 0) {
                  var index = details.indexOf(selected[0]);
                  details[index].posisi.push({
                    pos: item.posisi,
                  });
                } else {
                  var data = {
                    id_product: item.id_product,
                    nama_product: item.nama_product,
                    bahan_product: item.bahan_product,
                    warna: item.warna,

                    selesai: item.selesai,
                    total_pekerjaan: item.total_pekerjaan,
                    status: item.status,
                    posisi: [
                      {
                        pos: item.posisi,
                      },
                    ],
                  };
                  details.push(data);
                }
              }
              item.products = details;
              order.push(item);
              cb();
            } else {
              cb();
            }
          }
        });
      },
      function (e) {
        if (e) {
          return res.status(400).send({
            message: "cannot get data",
            data: order,
          });
        } else {
          return res.status(200).send({
            message: "successfully",
            orders: order,
          });
        }
      }
    );
  });
};

exports.get_orders_search = (req, res, next) => {
  var id_user = req.userData.id;
  let idorder = req.query.idorder;
  let nama_pasien = req.query.namapasien;
  let nama_customer = req.query.namacustomer.replace(/%20/g, " ");

  let order = idorder != "" ? "%" + idorder + "%" : "";
  let nama = nama_customer != "" ? "%" + nama_customer + "%" : "";
  let pasien = nama_pasien != "" ? "%" + nama_pasien + "%" : "";

  var sqlCekOrder =
    "select a.privilege, b.id, b.id_order, b.id_customer, b.nama_pasien, c.nama as customer, b.tanggal, b.tanggal_target_selesai, b.status from tbl_spk a LEFT JOIN tbl_order b ON a.id_order = b.id LEFT JOIN tbl_customers c ON b.id_customer = c.id WHERE a.id_pegawai = " +
    id_user +
    " and not b.id is NULL and (b.nama_pasien LIKE '" +
    pasien +
    "' or c.nama LIKE '" +
    nama +
    "' or b.id_order LIKE '" +
    order +
    "') ORDER BY b.tanggal_target_selesai DESC";

  console.log(sqlCekOrder);

  conn.query(sqlCekOrder, (err, rows) => {
    var order = [];
    var items = rows;
    async.eachSeries(
      items,
      function (item, cb) {
        var sqlCekProduct =
          "select a.id, a.id_order, a.id_product, b.nama as nama_product, a.warna, a.posisi_gigi as posisi, a.harga as harga_product, a.poin, a.garansi, c.nama as bahan_product, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product and status = 2) as selesai, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product ) as total_pekerjaan, a.status from tbl_order_detail a, tbl_product b, tbl_bahan c WHERE c.id = b.id_bahan AND b.id = a.id_product AND a.id_order = " +
          item.id +
          " and a.status !=2 order by a.posisi_gigi asc";
        conn.query(sqlCekProduct, (err, row) => {
          if (err) {
            return res.status(400).send({
              status: 400,
            });
          } else {
            if (row.length != 0) {
              details = [];
              for (let item of row) {
                var selected = details.filter(
                  (it) =>
                    it.warna == item.warna && it.id_product == item.id_product
                );
                if (selected.length > 0) {
                  var index = details.indexOf(selected[0]);
                  details[index].posisi.push({
                    pos: item.posisi,
                  });
                } else {
                  var data = {
                    id_product: item.id_product,
                    nama_product: item.nama_product,
                    bahan_product: item.bahan_product,
                    warna: item.warna,

                    selesai: item.selesai,
                    total_pekerjaan: item.total_pekerjaan,
                    status: item.status,
                    posisi: [
                      {
                        pos: item.posisi,
                      },
                    ],
                  };
                  details.push(data);
                }
              }
              item.products = details;
              order.push(item);
              cb();
            } else {
              cb();
            }
          }
        });
      },
      function (e) {
        if (e) {
          return res.status(400).send({
            message: "cannot get data",
            data: order,
          });
        } else {
          return res.status(200).send({
            message: "successfully",
            orders: order,
          });
        }
      }
    );
  });
};

exports.get_orders_datefilter = (req, res, next) => {
  var id_user = req.userData.id;
  var start = req.query.start;
  var end = req.query.end;

  var sqlCekOrder =
    "select a.privilege, b.id, b.id_order, b.id_customer, b.nama_pasien, c.nama as customer, b.tanggal, b.tanggal_target_selesai, b.status from tbl_spk a LEFT JOIN tbl_order b ON a.id_order = b.id LEFT JOIN tbl_customers c ON b.id_customer = c.id WHERE a.id_pegawai = " +
    id_user +
    " and (b.tanggal_selesai BETWEEN '" +
    start +
    "' AND '" +
    end +
    "') ORDER BY b.tanggal_target_selesai DESC";

  conn.query(sqlCekOrder, (err, rows) => {
    var order = [];
    var items = rows;
    async.eachSeries(
      items,
      function (item, cb) {
        var sqlCekProduct =
          "select a.id, a.id_order, a.id_product, b.nama as nama_product, a.warna, a.posisi_gigi as posisi, a.harga as harga_product, a.poin, a.garansi, c.nama as bahan_product, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product and status = 2) as selesai, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product ) as total_pekerjaan, a.status from tbl_order_detail a, tbl_product b, tbl_bahan c WHERE c.id = b.id_bahan AND b.id = a.id_product AND a.id_order = " +
          item.id +
          " and a.status !=2 order by a.posisi_gigi asc";
        conn.query(sqlCekProduct, (err, row) => {
          if (err) {
            return res.status(400).send({
              status: 400,
            });
          } else {
            if (row.length != 0) {
              details = [];
              for (let item of row) {
                var selected = details.filter(
                  (it) =>
                    it.warna == item.warna && it.id_product == item.id_product
                );
                if (selected.length > 0) {
                  var index = details.indexOf(selected[0]);
                  details[index].posisi.push({
                    pos: item.posisi,
                  });
                } else {
                  var data = {
                    id_product: item.id_product,
                    nama_product: item.nama_product,
                    bahan_product: item.bahan_product,
                    warna: item.warna,

                    selesai: item.selesai,
                    total_pekerjaan: item.total_pekerjaan,
                    status: item.status,
                    posisi: [
                      {
                        pos: item.posisi,
                      },
                    ],
                  };
                  details.push(data);
                }
              }
              item.products = details;
              order.push(item);
              cb();
            } else {
              cb();
            }
          }
        });
      },
      function (e) {
        if (e) {
          return res.status(400).send({
            message: "cannot get data",
            data: order,
          });
        } else {
          return res.status(200).send({
            message: "successfully",
            orders: order,
          });
        }
      }
    );
  });
};

/// History Order
exports.get_orders_history = (req, res, next) => {
  var id_user = req.userData.id;

  var sqlCekOrder =
    "select a.privilege, b.id, b.id_order, b.id_customer, b.nama_pasien, c.nama as customer, b.tanggal, b.tanggal_target_selesai, b.tanggal_selesai, b.status from tbl_spk a LEFT JOIN tbl_order b ON a.id_order = b.id LEFT JOIN tbl_customers c ON b.id_customer = c.id WHERE a.id_pegawai = " +
    id_user +
    " and b.status = 2 ORDER BY b.tanggal_selesai DESC";

  conn.query(sqlCekOrder, (err, rows) => {
    var order = [];
    var items = rows;
    async.eachSeries(
      items,
      function (item, cb) {
        var sqlCekProduct =
          "select a.id_product, b.nama as nama_product, (select count(id) from tbl_progress where id_order = a.id_order and id_product =  a.id_product and status = 2) as selesai, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product ) as total_pekerjaan from tbl_order_detail a LEFT JOIN tbl_product b ON a.id_product = b.id WHERE a.id_order = " +
          item.id +
          " GROUP BY a.id_product";
        conn.query(sqlCekProduct, (err, row) => {
          if (err) {
            return res.status(400).send({
              status: 400,
            });
          } else {
            item.products = row;
            order.push(item);
            cb();
          }
        });
      },
      function (e) {
        if (e) {
          return res.status(400).send({
            message: "cannot get data",
            data: order,
          });
        } else {
          return res.status(200).send({
            message: "successfully",
            orders: order,
          });
        }
      }
    );
  });
};

exports.get_orders_history_infinite = (req, res, next) => {
  var id_user = req.userData.id;
  var start = parseInt(req.query.start);
  var limit = parseInt(req.query.limit);

  var sqlCekOrder =
    "select a.privilege, b.id, b.id_order, b.id_customer, b.nama_pasien, c.nama as customer, b.tanggal, b.tanggal_target_selesai, b.tanggal_selesai, b.status from tbl_spk a LEFT JOIN tbl_order b ON a.id_order = b.id LEFT JOIN tbl_customers c ON b.id_customer = c.id WHERE a.id_pegawai = " +
    id_user +
    " and b.status = 2 ORDER BY b.tanggal_selesai DESC LIMIT ?,?";

  conn.query(sqlCekOrder, [start, limit], (err, rows) => {
    var order = [];
    var items = rows;
    async.eachSeries(
      items,
      function (item, cb) {
        var sqlCekProduct =
          "select a.id_product, b.nama as nama_product, c.nama as bahan_product, (select count(id) from tbl_progress where id_order = a.id_order and id_product =  a.id_product and status = 2) as selesai, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product ) as total_pekerjaan from tbl_order_detail a LEFT JOIN tbl_product b ON a.id_product = b.id LEFT JOIN tbl_bahan c ON b.id_bahan = c.id WHERE a.id_order = " +
          item.id +
          " GROUP BY a.id_product";
        conn.query(sqlCekProduct, (err, row) => {
          if (err) {
            return res.status(400).send({
              status: 400,
            });
          } else {
            item.products = row;
            order.push(item);
            cb();
          }
        });
      },
      function (e) {
        if (e) {
          return res.status(400).send({
            message: "cannot get data",
            data: order,
          });
        } else {
          return res.status(200).send({
            message: "successfully",
            orders: order,
          });
        }
      }
    );
  });
};

/// Pencarian History Order
exports.get_orders_history_search = (req, res, next) => {
  let id_user = req.userData.id;
  let idorder = req.query.idorder;
  let nama_pasien = req.query.namapasien;
  let nama_customer = req.query.namacustomer;

  let order = idorder != "" ? "%" + idorder + "%" : "";
  let nama = nama_customer != "" ? "%" + nama_customer + "%" : "";
  let pasien = nama_pasien != "" ? "%" + nama_pasien + "%" : "";

  let sqlCekOrder =
    "select a.privilege, b.id, b.id_order, b.id_customer, b.nama_pasien, c.nama as customer, b.tanggal, b.tanggal_target_selesai, b.tanggal_selesai, b.status from tbl_spk a LEFT JOIN tbl_order b ON a.id_order = b.id LEFT JOIN tbl_customers c ON b.id_customer = c.id WHERE a.id_pegawai = " +
    id_user +
    " and b.status = 2 AND (b.nama_pasien LIKE '" +
    pasien +
    "' or c.nama LIKE '" +
    nama +
    "' or b.id_order LIKE '" +
    order +
    "') ORDER BY b.tanggal_selesai DESC";

  conn.query(sqlCekOrder, (err, rows) => {
    var order = [];
    var items = rows;
    async.eachSeries(
      items,
      function (item, cb) {
        var sqlCekProduct =
          "select a.id_product, b.nama as nama_product, (select count(id) from tbl_progress where id_order = a.id_order and id_product =  a.id_product and status = 2) as selesai, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product ) as total_pekerjaan from tbl_order_detail a LEFT JOIN tbl_product b ON a.id_product = b.id WHERE a.id_order = " +
          item.id +
          " GROUP BY a.id_product";
        conn.query(sqlCekProduct, (err, row) => {
          if (err) {
            return res.status(400).send({
              status: 400,
            });
          } else {
            item.products = row;
            order.push(item);
            cb();
          }
        });
      },
      function (e) {
        if (e) {
          return res.status(400).send({
            message: "cannot get data",
            data: order,
          });
        } else {
          return res.status(200).send({
            message: "successfully",
            orders: order,
          });
        }
      }
    );
  });
};

exports.get_orders_history_datefilter = (req, res, next) => {
  let id_user = req.userData.id;
  var start = req.query.start;
  var end = req.query.end;

  var sqlCekOrder =
    "select a.privilege, b.id, b.id_order, b.id_customer, b.nama_pasien, c.nama as customer, b.tanggal, b.tanggal_target_selesai, b.tanggal_selesai, b.status from tbl_spk a LEFT JOIN tbl_order b ON a.id_order = b.id LEFT JOIN tbl_customers c ON b.id_customer = c.id WHERE a.id_pegawai = " +
    id_user +
    " and b.status = 2 and (b.tanggal_selesai BETWEEN '" +
    start +
    "' AND '" +
    end +
    "') ORDER BY b.tanggal_selesai DESC";

  console.log(sqlCekOrder);

  conn.query(sqlCekOrder, (err, rows) => {
    var order = [];
    var items = rows;
    async.eachSeries(
      items,
      function (item, cb) {
        var sqlCekProduct =
          "select a.id_product, b.nama as nama_product, c.nama as bahan_product, (select count(id) from tbl_progress where id_order = a.id_order and id_product =  a.id_product and status = 2) as selesai, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product ) as total_pekerjaan from tbl_order_detail a LEFT JOIN tbl_product b ON a.id_product = b.id LEFT JOIN tbl_bahan c ON b.id_bahan = c.id WHERE a.id_order = " +
          item.id +
          " GROUP BY a.id_product";
        conn.query(sqlCekProduct, (err, row) => {
          if (err) {
            return res.status(400).send({
              status: 400,
            });
          } else {
            item.products = row;
            order.push(item);
            cb();
          }
        });
      },
      function (e) {
        if (e) {
          return res.status(400).send({
            message: "cannot get data",
            data: order,
          });
        } else {
          return res.status(200).send({
            message: "successfully",
            orders: order,
          });
        }
      }
    );
  });
};

exports.get_progress_per_product = (req, res, next) => {
  var id_order = req.params.id_order;
  var id_product = req.params.id_product;

  conn.query(
    "select a.id_proses as idprogress, e.id_order, f.nama, a.id_order as id, a.id_product, a.status, a.tanggal_selesai, b.nama as nama_proses, b.approve_spv, b.nomor, c.nama as status_text, d.nama as nama_tekniker from tbl_progress a LEFT JOIN tbl_master_proses b ON a.id_proses = b.id LEFT JOIN tbl_status_progress c ON a.status = c.id LEFT JOIN tbl_pegawai d ON a.tekniker = d.id LEFT JOIN tbl_order e on a.id_order = e.id LEFT JOIN tbl_product f on a.id_product = f.id WHERE a.id_order = " +
      id_order +
      " AND a.id_product = " +
      id_product +
      " ORDER BY b.nomor ASC",
    (err, rows) => {
      if (err)
        res.status(400).send({
          msg: err,
          data: rows,
        });
      else
        res.status(200).send({
          msg: err,
          idorder: rows[0].id_order,
          nama_product: rows[0].nama,
          data: rows,
        });
    }
  );
};

exports.get_detail_order = (req, res, next) => {
  var id = req.params.id;
  conn.query(
    "select a.id, a.id_order, a.id_customer, a.nama_pasien, b.nama as customer, b.email as email_customer, b.no_hp as no_hp_customer, b.alamat as alamat_customer, b.poin as poin_customer, a.tanggal, a.tanggal_target_selesai, a.tanggal_selesai, a.status, a.ongkir, a.extra_charge, a.poin, a.kode_voucher, a.diskon, a.status_invoice, a.tanggal_invoice from tbl_order a LEFT JOIN tbl_customers b ON a.id_customer = b.id WHERE a.id = " +
      id,
    (err, rows) => {
      var order = rows.length > 0 ? rows[0] : {};
      var details = [];
      if (rows.length > 0) {
        conn.query(
          "select a.id, a.id_order, a.id_product, b.nama as nama_product, a.warna, a.posisi_gigi as posisi, a.harga as harga_product, a.poin, a.garansi, c.nama as bahan_product, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product and status = 2) as selesai, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product ) as total_pekerjaan, a.status from tbl_order_detail a, tbl_product b, tbl_bahan c WHERE c.id = b.id_bahan AND b.id = a.id_product AND a.id_order = " +
            order.id +
            " order by a.posisi_gigi asc",
          (err, row) => {
            details = [];
            for (let item of row) {
              var selected = details.filter(
                (it) =>
                  it.warna == item.warna && it.id_product == item.id_product
              );
              if (selected.length > 0) {
                var index = details.indexOf(selected[0]);
                details[index].posisi.push({
                  pos: item.posisi,
                });
              } else {
                var data = {
                  id_product: item.id_product,
                  nama_product: item.nama_product,
                  bahan_product: item.bahan_product,
                  warna: item.warna,

                  selesai: item.selesai,
                  total_pekerjaan: item.total_pekerjaan,
                  status: item.status,
                  posisi: [
                    {
                      pos: item.posisi,
                    },
                  ],
                };
                details.push(data);
              }
            }

            res.json({
              order: order,
              details: details,
            });
          }
        );
      } else {
        res.json({
          order: order,
          details: details,
        });
      }
    }
  );
};

exports.get_tekniker_in_order = (req, res, next) => {
  var idorder = req.params.id;
  var query =
    "select pegawai.id, pegawai.nama, pegawai.privilege, pegawai.token from tbl_order a LEFT JOIN tbl_customers b ON a.id_customer = b.id LEFT JOIN tbl_spk spk on spk.id_order = a.id LEFT JOIN tbl_pegawai pegawai on pegawai.id = spk.id_pegawai WHERE a.id = " +
    idorder +
    " ";

  conn.query(query, (err, rows) => {
    if (err) {
      return res.status(400).send({
        status: 400,
        data: rows,
      });
    } else {
      return res.status(200).send({
        status: 200,
        data: rows,
      });
    }
  });
};

exports.get_detail_order_progress = (req, res, next) => {
  var id = req.params.id;

  conn.query(
    "select a.id, a.id_order, a.id_customer, a.nama_pasien, b.nama as customer, a.tanggal, a.tanggal_target_selesai, a.status from tbl_order a LEFT JOIN tbl_customers b ON a.id_customer = b.id WHERE a.id = " +
      id,
    (err, rows) => {
      var item = rows.length > 0 ? rows[0] : {};
      if (rows.length > 0) {
        conn.query(
          "select a.id_product, a.warna, a.posisi_gigi, b.nama as nama_product, (select count(id) from tbl_progress where id_order = a.id_order and id_product =  a.id_product and status = 2) as selesai, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product ) as total_pekerjaan from tbl_order_detail a LEFT JOIN tbl_product b ON a.id_product = b.id WHERE a.id_order = " +
            item.id +
            " GROUP BY a.id_product",
          (err, row) => {
            item.products = row;
            res.status(200).send(item);
          }
        );
      } else
        res.status(404).send({
          data: item,
        });
    }
  );
};

exports.updateProgress = function (req, res) {
  var id_order = req.body.id_order;
  var id_product = req.body.id_product;
  var id_progress = req.body.id_progress;
  var status = req.body.status;
  var tanggal_selesai = req.body.tanggal_selesai;
  var id_tekniker = req.body.id_tekniker;

  conn.query(
    "Update tbl_progress a LEFT JOIN tbl_master_proses b ON a.id_proses = b.id LEFT JOIN tbl_status_progress c ON a.status = c.id LEFT JOIN tbl_pegawai d ON a.tekniker = d.id SET a.status = " +
      status +
      ", a.tekniker = " +
      id_tekniker +
      ", a.tanggal_selesai = " +
      tanggal_selesai +
      " WHERE a.id_proses= " +
      id_progress +
      " AND a.id_order = " +
      id_order +
      " AND a.id_product = " +
      id_product,
    function (error, rows) {
      if (error) res.status(400).json(error);
      else res.json(rows);
    }
  );
};

exports.qualityControl = function (req, res) {
  var id_order = req.body.id_order;
  var id_product = req.body.id_product;
  var margin = req.body.margin;
  var oklusi = req.body.oklusi;
  var anatomi = req.body.anatomi;
  var warna = req.body.warna;
  var pic = req.body.pic;
  var tanggal = req.body.tanggal;

  var sql =
    "INSERT INTO tbl_quality_control (id_order, id_product, margin, oklusi, anatomi, warna, pic, tanggal) VALUES (" +
    id_order +
    "," +
    id_product +
    "," +
    margin +
    "," +
    oklusi +
    "," +
    anatomi +
    "," +
    warna +
    "," +
    pic +
    "," +
    tanggal +
    ")";

  conn.query(sql, function (err, result) {
    if (err) {
      return res.status(400).send({
        status: 400,
      });
    } else {
      return res.status(200).send({
        status: 200,
      });
    }
  });
};

exports.updateFCMToken = (req, res) => {
  var token = req.body.token;
  var id = req.body.id;

  var sql = "UPDATE tbl_pegawai SET token= ? WHERE id= ?";

  conn.query(sql, [token, id], (err, result) => {
    if (err) {
      return res.status(400).send({
        status: 400,
      });
    } else {
      return res.status(200).send({
        status: 200,
      });
    }
  });
};

exports.cekQuality = (req, res, next) => {
  var id_order = req.params.id_order;
  var id_product = req.params.id_product;

  var sql =
    "SELECT id FROM tbl_quality_control WHERE id_order = " +
    id_order +
    " AND id_product = " +
    id_product +
    "";

  conn.query(sql, (err, rows) => {
    if (rows.length < 1) {
      res.status(404).send({
        status: false,
        data: rows,
      });
    } else
      res.status(200).send({
        status: true,
        data: rows,
      });
  });
};

exports.updateStatusProduk = (req, res, next) => {
  var id_order = req.body.id_order;
  var id_product = req.body.id_product;
  var sql =
    "UPDATE tbl_order_detail set status = 2 WHERE id_order = " +
    id_order +
    " AND id_product = " +
    id_product +
    "";

  console.log(sql);

  conn.query(sql, (err, rows) => {
    if (rows.changedRows <= 0) {
      res.status(404).send({
        status: false,
      });
    } else if (rows.changedRows > 0) {
      res.status(200).send({
        status: true,
      });
    } else {
      res.status(400).send({
        status: false,
      });
    }
  });
};

exports.absensiAwal = function (req, res) {
  var id_pegawai = req.body.id_pegawai;
  var tanggal = req.body.tanggal;
  var masuk = req.body.masuk;

  var sql =
    "INSERT INTO tbl_absensi (id_pegawai, tanggal, masuk) VALUES (" +
    id_pegawai +
    "," +
    tanggal +
    "," +
    masuk +
    ")";

  conn.query(sql, function (err, result) {
    if (err) {
      return res.status(400).send({
        status: 400,
      });
    } else {
      return res.status(200).send({
        status: 200,
      });
    }
  });
};

exports.absensiAkhir = (req, res, next) => {
  var id_pegawai = req.body.id_pegawai;
  var tanggal = req.body.tanggal;
  var keluar = req.body.keluar;
  var sql =
    "UPDATE tbl_absensi set keluar = " +
    keluar +
    " WHERE id_pegawai = " +
    id_pegawai +
    " AND tanggal = " +
    tanggal +
    "";

  conn.query(sql, (err, rows) => {
    if (rows.changedRows <= 0) {
      res.status(404).send({
        status: 404,
      });
    } else if (rows.changedRows > 0) {
      res.status(200).send({
        status: 200,
      });
    } else {
      res.status(400).send({
        status: 400,
      });
    }
  });
};

exports.dataabsensi = (req, res, next) => {
  var id_pegawai = req.params.id;
  console.log(id_pegawai);
  var sql = "SELECT * FROM tbl_absensi WHERE id_pegawai = ?";

  conn.query(sql, [id_pegawai], (err, rows) => {
    if (rows.length < 1) {
      res.status(404).send({
        status: false,
        data: rows,
      });
    } else
      res.status(200).send({
        status: true,
        data: rows,
      });
  });
};

exports.profileTekniker = (req, res, next) => {
  var id_pegawai = req.params.id;
  console.log(id_pegawai);
  var sql =
    "SELECT id, nama, email, foto, privilege, token FROM tbl_pegawai WHERE id = ?";

  conn.query(sql, [id_pegawai], (err, rows) => {
    if (rows.length < 1) {
      res.status(404).send({
        data: rows[0],
      });
    } else {
      res.status(200).send({
        data: rows[0],
      });
    }
  });
};
