const express = require('express');
const router = express.Router();
const teknikerRoutes = require('./routes/tekniker')
const authRoutes = require('./routes/auth')
const ordersRoutes = require('./routes/orders')
const customerRoutes = require('./routes/customer')
const notifikasiRoutes = require('./routes/notifikasi')
const chatsRoutes = require('./routes/chats')
const qualityControlRoutes = require('./routes/quality_control')

router.use('/tekniker', teknikerRoutes)
router.use('/auth', authRoutes)
router.use('/orders', ordersRoutes)
router.use('/customer', customerRoutes)
router.use('/notifikasi', notifikasiRoutes)
router.use('/chat', chatsRoutes)
router.use('/quality-control', qualityControlRoutes)

module.exports = router;