const express = require('express');
const router = express.Router();
const qualityCtrl = require('../controllers/quality_control')
const variables = require('../../variables')
const multer = require('multer')

var storageFileImage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, variables.PATH + '/assets/images')
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname)
    }
})

var uploadImages = multer({ storage: storageFileImage })
// mendapatkan data quality control per customer, params id adalah id customer
router.get('/lists/:id', qualityCtrl.get_list_qc_customer)

// mendapatkan data quality control berdasarkan id quality control
router.get('/qc/:id', qualityCtrl.get_quality_control_single)

// rating & evaluasi
// router.get('/all/:page/:urutkan', qualityCtrl.all_ratings)
router.get('/review/:idorder', qualityCtrl.review_order)

//TODO butuh router untuk mengetahui order detail yang telah mendapatkan rating&review sesuai kriteria
router.get('/need-evaluasi', qualityCtrl.get_evaluasi)
router.post('/review-add', uploadImages.array('images'), qualityCtrl.review_add)
module.exports = router