const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth')
const teknikerCtrl = require('../controllers/tekniker')


/* data untuk halaman/tab Works per tekniker/spv */
router.get('/orders', checkAuth, teknikerCtrl.get_orders)
router.get('/orders-infinity', checkAuth, teknikerCtrl.get_orders_infinite)
router.get('/orders-search', checkAuth, teknikerCtrl.get_orders_search)
router.get('/orders-filter', checkAuth, teknikerCtrl.get_orders_datefilter)

router.get('/orders-riwayat', checkAuth, teknikerCtrl.get_orders_history)
router.get('/orders-riwayat-infinity', checkAuth, teknikerCtrl.get_orders_history_infinite)
router.get('/orders-riwayat-search', checkAuth, teknikerCtrl.get_orders_history_search)
router.get('/orders-riwayat-filter', checkAuth, teknikerCtrl.get_orders_history_datefilter)


/* data list pekerjaan per product & order */
router.get('/progress/:id_order/:id_product', checkAuth, teknikerCtrl.get_progress_per_product)

/* Router cek quality control */
router.get('/cek-quality/:id_order/:id_product', checkAuth, teknikerCtrl.cekQuality)

/* data detail order (termasuk detail item product) */
router.get('/order/:id', checkAuth, teknikerCtrl.get_detail_order)

// list tekniker di setiap order
router.get('/teknikerinorder/:id', checkAuth, teknikerCtrl.get_tekniker_in_order)

/* data untuk halaman landing setelah scan QR Code */
router.get('/progress-order/:id', checkAuth, teknikerCtrl.get_detail_order_progress)

router.put('/update-progress/', checkAuth, teknikerCtrl.updateProgress)

router.post('/quality-control/', checkAuth, teknikerCtrl.qualityControl)

router.put('/updatefcm/', checkAuth, teknikerCtrl.updateFCMToken)

router.put('/update-produk-selesai/', checkAuth, teknikerCtrl.updateStatusProduk)

//Absensi
router.post('/absensi-awal/', checkAuth, teknikerCtrl.absensiAwal)
router.put('/absensi-akhir/', checkAuth, teknikerCtrl.absensiAkhir)
router.get('/absensi/:id', checkAuth, teknikerCtrl.dataabsensi)

//Profile
router.get('/profile/:id', checkAuth, teknikerCtrl.profileTekniker)

module.exports = router;